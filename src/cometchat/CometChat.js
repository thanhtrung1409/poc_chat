import React, {useEffect, useState} from 'react';
import { UIKitSettingsBuilder } from "@cometchat/uikit-shared";
import { CometChatUIKit } from "@cometchat/chat-uikit-react";
import { CometChatUsersWithMessages } from "@cometchat/chat-uikit-react";
import {LoadingIndicator} from "stream-chat-react";

const COMETCHAT_CONSTANTS = {
  APP_ID: "2555718196ef226b", //Replace with your App ID
  REGION: "US", //Replace with your App Region
  AUTH_KEY: "fb8b32c58ae40b2d11ac14c505ce70cce9b11b63", //Replace with your Auth Key
};

const UIKitSettings = new UIKitSettingsBuilder()
  .setAppId(COMETCHAT_CONSTANTS.APP_ID)
  .setRegion(COMETCHAT_CONSTANTS.REGION)
  .setAuthKey(COMETCHAT_CONSTANTS.AUTH_KEY)
  .subscribePresenceForFriends()
  .build();

const CometChat = () => {
  const UID = "1"; // Loc
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    CometChatUIKit.init(UIKitSettings).then(async response => {
      console.log("Initialization completed successfully", response);
      await login();
      setLoading(false);
    }).catch(error => {
      console.error("Initialization failed with error:", error);
      setLoading(false);
    });
  }, []);


  function login() {
    CometChatUIKit.getLoggedinUser().then((user) => {
      console.log('1111', user);
      if (!user) {
        //Login user
        CometChatUIKit.login(UID)
          .then((user) => {
            console.log("Login Successful:", { user });
            //mount your app
          })
          .catch(console.log);
      } else {
        //mount your app
      }
    });
  }

  if (loading) {
    return (
      <div className="button-container">
        <LoadingIndicator />
      </div>
    );
  }


  return <CometChatUsersWithMessages />;
};

export default CometChat;