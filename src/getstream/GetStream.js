import React, {useState} from 'react';
import { StreamChat } from 'stream-chat';
import {
  Chat,
  Channel,
  ChannelHeader,
  MessageInput,
  MessageList,
  Thread,
  Window, LoadingIndicator, ChannelList, useChatContext,
} from 'stream-chat-react';
import 'stream-chat-react/dist/css/v2/index.css';
import 'stream-chat-react/dist/css/v2/index.css';
import '../layout.css';
import '../Home.css'

import { useClient } from '../hooks/useClient';

const userId = 'locpham';
const userName = 'locpham';
const user = {
  id: userId,
  name: userName,
  image: `https://getstream.io/random_png/?id=${userId}&name=${userName}`,
};
const apiKey = 'u8rema9wba84';
const chatClient = new StreamChat(apiKey);
const userToken = chatClient.devToken(userId);

const sort = { last_message_at: -1 };
const filters = {
  type: 'messaging',
  // members: { $in: [userId] },
  // name: 'test1'
};

const CometChat = () => {
  const { client } = useChatContext();
  const [filter, setFilter] = useState(filters);
  const chatClient = useClient({
    apiKey,
    user,
    tokenOrProvider: userToken,
  });

  if (!chatClient) {
    return (
      <div className="button-container">
        <LoadingIndicator />
      </div>
    );
  }


  return (
    <Chat client={chatClient} theme='str-chat__theme-light'>
      <div className="channel-section">
        <ChannelList filters={filter} sort={sort} showChannelSearch/>
      </div>
      <Channel>
        <Window>
          <ChannelHeader/>
          <MessageList/>
          <MessageInput/>
        </Window>
        <Thread/>
      </Channel>
    </Chat>
  );
};

export default CometChat;