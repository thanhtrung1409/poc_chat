

export const customSearchFunction = async (
  props,
  event,
  client,
) => {
  const { setResults, setSearching, setQuery } = props;
  const value = event.target.value;

  const filters = {
    name: { $autocomplete: value },
    members: { $in: client.userID },
  };

  setSearching(true);
  setQuery(value);
  const channels = await client.queryChannels(filters);
  setResults(channels);
  setSearching(false);
};