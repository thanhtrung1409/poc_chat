import React from "react";
import {createBrowserRouter,  RouterProvider} from "react-router-dom";
import Home from "./Home";
import CometChat from "./cometchat/CometChat";
import GetStream from "./getstream/GetStream";

export default function RootWrapper() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <Home />,
        },
        {
            path: "/cometchat",
            element: <CometChat />,
        },
        {
            path: "/getstream",
            element: <GetStream />,
        },
    ]);


    return (
        <RouterProvider router={router} />
    );
}
