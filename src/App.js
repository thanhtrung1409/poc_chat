import './App.css';
import RootWrapper from "./routes";
function App() {
  return (
      <RootWrapper />
  );
}

export default App;
