import React from 'react';
import {useNavigate} from "react-router-dom";
import './Home.css';

const Home = (props) => {
    const navigate = useNavigate();
    return (
        <div className="button-container">
            <button className="large-button" onClick={() => navigate('/cometchat')}>Go to Cometchat</button>
            <button className="large-button" onClick={() => navigate('/getstream')}>Go to Getstream</button>
        </div>
    );
}

export default Home;